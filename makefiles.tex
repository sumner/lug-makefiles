\documentclass{lug}

\usepackage{fontawesome}
\usepackage{etoolbox}
\usepackage{textcomp}
\usepackage[nodisplayskipstretch]{setspace}
\usepackage{xspace}
\usepackage{verbatim}
\usepackage{multicol}
\usepackage{soul}
\usepackage{attrib}

\usepackage{amsmath,amssymb,amsthm}

\usepackage[linesnumbered,commentsnumbered,ruled,vlined]{algorithm2e}
\newcommand\mycommfont[1]{\footnotesize\ttfamily\textcolor{blue}{#1}}
\SetCommentSty{mycommfont}
\SetKwComment{tcc}{ \# }{}
\SetKwComment{tcp}{ \# }{}

\usepackage{siunitx}

\usepackage{tikz}
\usepackage{pgfplots}
\usetikzlibrary{decorations.pathreplacing,calc,arrows.meta,shapes,graphs}

\AtBeginEnvironment{minted}{\singlespacing\fontsize{10}{10}\selectfont}
\usefonttheme{serif}

\makeatletter
\patchcmd{\beamer@sectionintoc}{\vskip1.5em}{\vskip0.5em}{}{}
\makeatother

% Math stuffs
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\lcm}{\text{lcm}}
\newcommand{\Inn}{\text{Inn}}
\newcommand{\Aut}{\text{Aut}}
\newcommand{\Ker}{\text{Ker}\ }
\newcommand{\la}{\langle}
\newcommand{\ra}{\rangle}

\newcommand{\yournewcommand}[2]{Something #1, and #2}

\newenvironment{question}[1]{\par\textbf{Question #1.}\par}{}

\newcommand{\pmidg}[1]{\parbox{\widthof{#1}}{#1}}
\newcommand{\splitslide}[4]{
    \noindent
    \begin{minipage}{#1 \textwidth - #2 }
        #3
    \end{minipage}%
    \hspace{ \dimexpr #2 * 2 \relax }%
    \begin{minipage}{\textwidth - #1 \textwidth - #2 }
        #4
    \end{minipage}
}

\newcommand{\frameoutput}[1]{\frame{\colorbox{white}{#1}}}

\newcommand{\tikzmark}[1]{%
\tikz[baseline=-0.55ex,overlay,remember picture] \node[inner sep=0pt,] (#1)
{\vphantom{T}};
}

\newcommand{\braced}[3]{%
    \begin{tikzpicture}[overlay,remember picture]
        \draw [thick,decorate,decoration={brace,raise=1ex,amplitude=4pt},blue] (#2.south west-|T1.south west) -- node[anchor=west,left,xshift=-1.8ex,text=olive]{#3} (#1.north west-|T1.south west);
    \end{tikzpicture}
}

\newcommand{\make}{GNU \texttt{make}\xspace}

\title{\texttt{make} it Rain}
\author{Sumner Evans}
\institute{Mines Linux Users Group}

\begin{document}

\section{Introduction}

\begin{frame}{What is \make?}
    \make is a tool which controls the generation of executables and other
    non-source files of a program from the program's source files.

    \make gets its knowledge of how to build your program from a file called the
    \textit{makefile}, which lists each of the non-source files and how to
    compute it from other files.

    \attrib{The \make Project
        Page\footnote[frame]{\url{https://www.gnu.org/software/make/}}}
\end{frame}

\begin{frame}{Basic Usage of \make}
    \texttt{\$ make}
\end{frame}

\begin{frame}[fragile]{Advanced Usage of \make}
    To run \texttt{make}, there must be a file called \texttt{GNUmakefile},
    \texttt{makefile}, or \texttt{Makefile} in the current working directory.
    Otherwise, it will return an error.

    The \texttt{make} program takes an arbitrary number of parameters. Each of
    the parameters must be a \textit{target}\footnote[frame]{More on targets
        later}.

    If no parameter is specified, the \texttt{all} target is assumed.

    \pause
    \textbf{Examples:}

    \begin{minted}{bash}
        $ make all
        $ make a.out README.pdf
        $ make makefiles.pdf
        $ make profit
    \end{minted}
\end{frame}

\section{How to Make a \texttt{Makefile}}

\begin{frame}[fragile]{Understanding the Building Blocks of \texttt{Makefile}s}
    The main building block of a \texttt{Makefile} is the \textit{rule}.
    Every \texttt{Makefile} contains a set of rules.

    \pause
    All rules are of the form

    \begin{minted}{makefile}
        target: dependencies ...
                commands
                ...
    \end{minted}

    \textit{Note:} commands must be indented with the \texttt{TAB} character.

    \pause
    \begin{itemize}
        \item The \texttt{target} is the file to be created by the rule.
        \item The \texttt{dependencies} (also known as prerequisites) are a list
            of the targets/files which need to exist for the \texttt{target} to
            be created.
        \item The \texttt{commands} are a list of the commands to create the
            \texttt{target} from the \texttt{dependencies}.
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{A Very Simple Example}
    \begin{minted}{makefile}
        all: helloworld

        helloworld: helloworld.c
                gcc -o helloworld helloworld.c
    \end{minted}
    \vspace{1em}
    In this example, to build \texttt{helloworld}, \make will ensure that the
    \texttt{helloworld.c} file exists.
\end{frame}

\begin{frame}[fragile]{Why is this Better than a Bash Script?}
    But wait, I can do that with a BASH script:
    \begin{minted}{bash}
        [[ -f helloworld.c ]] && gcc -o helloworld helloworld.c
    \end{minted}
    why do I need a \texttt{Makefile}?

    \pause
    \textbf{\make only executes a target's commands if its \textit{dependencies}
        have been updated since the last time \texttt{make} was called.}
\end{frame}

\begin{frame}[fragile]{Dependency Management}
    Dependency management is one of the best features of \make. Dependencies can
    even be chained!
    \begin{minted}{makefile}
        all: presentation.pdf

        presentation.pdf: presentation.tex
                xelatex -shell-escape presentation.tex

        presentation.tex: presentation.rst xelatex.tex
                rst2beamer --template=xelatex.tex \
                    presentation.rst > presentation.tex
    \end{minted}
\end{frame}

\begin{frame}[fragile]{\texttt{.PHONY}}
    \make expects all targets to be \textit{files or directories}. So if you
    have a file called \texttt{all}, you may run into problems.

    To avoid this, you can define targets as being \textit{phony}. This will
    basically make it so that those targets are perpetually out of date and will
    always be recomputed, regardless of whether or not they exist on the
    filesystem.

    \begin{minted}{makefile}
        all: foo bar baz

        .PHONY: all
    \end{minted}
\end{frame}

\begin{frame}[fragile]{A \texttt{Makefile} is not a Bash Script!}
    Do \textbf{not} write a \texttt{Makefile} that looks like this:

    \begin{minted}{makefile}
        all: documentation
                gcc helloworld.c

        documentation:
                xelatex -shell-escape doc.tex
                biber doc
                xelatex -shell-escape doc.tex
    \end{minted}

    This \texttt{Makefile} will cause the program and the documentation to be
    compiled \textit{regardless} of whether or not you've updated the
    corresponding source files (\texttt{doc.tex} and \texttt{helloworld.c}).

    This is very bad if you are compiling a sizeable program.
\end{frame}

\begin{frame}[fragile,allowframebreaks]{Automatic Variables and Implicit Rules}
    Often, you may have many files which have the same basic rule. For example
    you may want to compile a \texttt{.o} file for every \texttt{.cpp} file in a
    C++ project. Luckily, there's a syntax for that:
    \begin{minted}{makefile}
        %.o: %.cpp
                g++ -c $< -o $@
    \end{minted}

    \begin{itemize}
        \item \texttt{\%.o} as the \textit{target} is a wildcard for all
            \texttt{*.o} targets.
        \item \texttt{\%.cpp} means that if the target is \texttt{foo.o}, then
            it depends on \texttt{foo.cpp}.
        \item \texttt{\$@} is the file name of the \textit{target} of the rule.
        \item \texttt{\$<} is the file name of the first prerequisite.
    \end{itemize}

    \framebreak

    There are a lot of other useful automatic variables.
    \begin{itemize}
        \item \texttt{\$@} is the file name of the \textit{target} of the rule.
        \item \texttt{\$<} is the file name of the first prerequisite.
        \item \texttt{\$?} is the file names of all the prerequisites that are
            newer than the target, with spaces between them.
        \item \texttt{\$\^{}} is the names of all the prerequisites, with spaces
            between them.
        \item A bunch more\dots\ See
            \url{https://www.gnu.org/software/make/manual/html_node/Automatic-Variables.html}
            for a list.
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Variables}
    If there are \textit{automatic} variables, then there must be normal
    variables too, right? Yes!

    You can define variables with the following syntax:
    \begin{minted}{makefile}
        CXX=clang
        OUTFILES=foo.o bar.o baz.o
    \end{minted}
    and you can use them like this (\textit{note, parentheses are required}):
    \begin{minted}{makefile}
        all: $(OUTFILES)

        %.o: %.c
                $(CXX) $< -o $@
    \end{minted}
\end{frame}

\begin{frame}[fragile]{Wildcards and Pattern Substitutions}
    Often it is useful to have glob-like wildcards in variables. To do this, you
    can use the \texttt{wildcard} function:
    \begin{minted}{makefile}
        TEXFILES=$(wildcard *.tex)
    \end{minted}
    \pause
    You may then want to use all of those filenames to determine the targets to
    compute. To do this, you can use the \texttt{patsubst} (\textbf{pat}tern
    \textbf{subst}itution) function:
    \begin{minted}{makefile}
        PDFFILES=$(patsubst %.tex,out/%.pdf,$(TEXFILES))
    \end{minted}

    \pause
    This will result in \texttt{TEXFILES} containing all of the \texttt{.tex}
    files in the directory, and \texttt{PDFFILES} containing all of those file
    names, but with \texttt{.pdf} instead of \texttt{.tex}.
\end{frame}

\begin{frame}[fragile]{Managing TAR Files}
    You can use \make to manage TAR archives. This will create a
    \texttt{.tar.gz} containing all of the \texttt{.c} files in the directory.
    \begin{minted}{makefile}
        CFILES=$(wildcard *.c)

        all: test($(CFILES))

        test(%.c):
                ar cr $@ $%
    \end{minted}

    Note here that \texttt{\$\%} is another automatic variable containing the
    target member name. It is empty if the target is not an archive member.
\end{frame}

\section{Examples}

\begin{frame}[fragile]{Compiling a Presentation}
    \begin{minted}{makefile}
        FILENAME=makefiles
        LATEX_COMPILER=xelatex -shell-escape

        all: $(FILENAME).pdf

        examples/%.pdf: examples/%.tex
                $(LATEX_COMPILER) -output-directory=examples $<

        %.pdf: %.tex lug.cls
                $(LATEX_COMPILER) $<

        .PHONY: all
    \end{minted}
\end{frame}

\begin{frame}[fragile]{Compiling a reStructuredText Presentation}
    \begin{minted}{makefile}
        RSTFILES=$(wildcard *.rst)
        PDFFILES=$(patsubst %.rst,out/%.pdf,$(RSTFILES))

        .PHONY: all
        all: $(PDFFILES)

        out:
                mkdir out

        out/%.pdf: %.tex beamerthemecsam.sty csam.pdf | out
                xelatex -shell-escape $<
                mv $(patsubst out/%.pdf,%.pdf,$@) out

        %.tex: %.rst xelatex.tex
                rst2beamer --template=xelatex.tex \
                    --theme=csam $< > $@
    \end{minted}
\end{frame}

\begin{frame}{Additional Resources}
    \begin{itemize}
        \item The GNU \texttt{make} Manual:
            \url{https://www.gnu.org/software/make/manual/html_node/index.html}
        \item Unix Makefile Tutorial:
            \url{https://www.tutorialspoint.com/makefile/}
    \end{itemize}
\end{frame}

\end{document}
% Local Variables:
% TeX-command-extra-options: "-shell-escape"
% End:
