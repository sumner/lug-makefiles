``make`` it rain
################
A Presentation About Makefiles for LUG
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Authors/Presenters
------------------

- `Sumner Evans`_

.. _Sumner Evans: https://gitlab.com/sumner

Topics Covered
--------------

- What is GNU Make?
- Basic Usage of ``make``
- How to Make a ``Makefile``

  - Understanding the components of a ``Makefile`` (rules, targets)
  - Understanding chaining dependencies
  - Macros (``$@``, ``@<``, etc.)
  - ``.phony``

- Examples of useful ``Makefile`` tricks

  - ``wildcard``
  - ``patsubst``

Resources
---------

- Unix Makefile Tutorial: https://www.tutorialspoint.com/makefile/
